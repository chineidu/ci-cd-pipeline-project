# A sample .gitlab-ci.yml

```yaml
# sample .gitlab-ci.yml file
image: node:10

stages:
  - build
  - test
  - deploy
  - deployment tests

cache:
  key: ${CL_COMMIT_REF_SLUG}
  paths:
    - node_modules/  # This directory saves all the node dependencies i.e modules, packages, libraries, etc

# Jobs 
# Job 1
build website:
  stage: build
  script:
    - echo $CL_COMMIT_SHORT_SHA
    - npm install
    - npm install -g gatsby-cli
    - gatsby build
    - other gatsby commands
  artifacts:  # saves the output from the build stage and it can be re-used in other stages.
    paths:
      - ./public

# Job 2
test artifact:
  image: alpine
  stage: test
  script:
    - grep -q "Gatsby" ./public/index.html

# Job 3
test website:
  stage: test
  script:
    - npm install
    - npm install -g gatsby-cli
    - gatsby serve &
    - other gatsby commands

# Job 4
deploy to surge:
  stage: deploy
  script:
    - npm install --global surge
    - surge --project ./public - domain instazone.surge.sh

# Job 5
test deployment:
  image: alpine
  stage: deployment tests
  script:
    - apk add --no cache curl
    - other commands

```
